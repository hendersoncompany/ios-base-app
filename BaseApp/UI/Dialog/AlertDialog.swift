//
//  AlertDialog.swift
//  BaseApp
//
//  Created by Henderson on 2021/04/08.
//  Copyright © 2021 Henderson. All rights reserved.
//

import UIKit
import RxSwift

class AlertDialog: UIViewController {
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var alertMessage: CustomLabel!
    @IBOutlet weak var cancelButton: CustomButton!
    @IBOutlet weak var okButton: CustomButton!
    
    //Local Variable
    var message = ""
    var hasCancel = false
    var cancelEvent: (() -> Void)? = nil
    var okEvent: (() -> Void)? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initView()
    }
    
    func initAlert(message: String) {
        self.message = message
        modalPresentationStyle = .overCurrentContext
        modalTransitionStyle = .crossDissolve
    }
    
    func initAlert(message: String, okEvent: @escaping () -> Void, hasCancel: Bool = false) {
        self.okEvent = okEvent
        self.hasCancel = hasCancel
        initAlert(message: message)
    }
    
    private func initView() {
        alertMessage.text = message
        cancelButton.isHidden = !hasCancel
        
        okButton.rx.tap.bind {
            if let event = self.okEvent {
                event()
                self.dismiss(animated: false, completion: nil)
            } else {
                self.dismiss(animated: false, completion: nil)
            }
        }.disposed(by: disposeBag)
        
        cancelButton.rx.tap.bind {
            self.dismiss(animated: false, completion: nil)
        }.disposed(by: disposeBag)
    }
}
