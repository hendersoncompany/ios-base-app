//
//  BottomSheetDialog.swift
//  BaseApp
//
//  Created by Henderson on 2021/04/08.
//  Copyright © 2021 Henderson. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol BottomSheetDialogDelegate {
    func dismissBottomSheet(dialog: UIViewController)
    func complete(data: Any?)
}

class BaseBottomSheetDialog: BaseViewController, UIGestureRecognizerDelegate {
    var delegate: BottomSheetDialogDelegate? = nil
    
    //Constants for animation
    let animationDuration = 0.25
    
    let fullView: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addListenerDismissDialog()
        addPanGesture()
    }
    
    private func addPanGesture() {
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(panGesture))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
    }
    
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
        
        let y = self.view.frame.minY
        if (y + translation.y >= fullView) {
            self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        
        if recognizer.state == .ended {
            UIView.animate(withDuration: animationDuration, delay: 0.0, options: [.allowUserInteraction], animations: {
                if  velocity.y >= 0 {
                    self.delegate?.dismissBottomSheet(dialog: self)
                } else {
                    self.view.frame = CGRect(x: 0, y: self.fullView, width: self.view.frame.width, height: self.view.frame.height)
                }
            })
        }
    }

    /// parentView 터치 시에 다이얼로그 닫히도록 이벤트 등록
    func addListenerDismissDialog(){
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.clickedParentView(sender:)))
        self.view.addGestureRecognizer(gesture)
    }
    
    @objc func clickedParentView(sender:UIGestureRecognizer) {
        delegate?.dismissBottomSheet(dialog: self)
    }
}
