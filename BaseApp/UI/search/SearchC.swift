//
//  MvvmVC.swift
//  BaseApp
//
//  Created by Henderson on 2020/06/16.
//  Copyright © 2020 Henderson. All rights reserved.
//

import UIKit

import RxCocoa
import RxSwift

enum TableViewCellType {
    case error(message: String)
    case empty
}

class SearchC: BaseVC, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var shownCities = [String]()
    let allCities = ["New York", "London", "Oslo", "Warsaw", "Berlin", "Praga"]
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let nibName = UINib(nibName: "TestCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "testCell")
        
        searchBar.rx.text // RxCocoa의 Observable 속성
            .orEmpty // 옵셔널이 아니도록 만듭니다.
            .subscribe(onNext: { [unowned self] query in // 이 부분 덕분에 모든 새로운 값에 대한 알림을 받을 수 있습니다.
                self.shownCities = self.allCities.filter { $0.hasPrefix(query) } // 도시를 찾기 위한 “API 요청” 작업을 합니다.
                self.tableView.reloadData() // 테이블 뷰를 다시 불러옵니다.
            })
            .disposed(by: disposeBag)
        
//        searchBar.rx.text // RxCocoa의 Observable 속성
//            .orEmpty // 옵셔널이 아니도록 만듭니다.
//            .debounce(0.5, scheduler: MainScheduler.instance) // Wait 0.5 for changes.
//            .distinctUntilChanged() // 새로운 값이 이전의 값과 같은지 확인합니다.
//            .filter { !$0.isEmpty } // 새로운 값이 정말 새롭다면, 비어있지 않은 쿼리를 위해 필터링합니다.
//            .subscribe(onNext: { [unowned self] query in // Here we subscribe to every new value, that is not empty (thanks to filter above).
//                self.shownCities = self.allCities.filter { $0.hasPrefix(query) } // 도시를 찾기 위한 “API 요청” 작업을 합니다.
//                self.tableView.reloadData() // 테이블 뷰를 다시 불러옵니다.
//            })
//            .disposed(by: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shownCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "testCell", for: indexPath) as! TestCell
        cell.testCellTitle.text = shownCities[indexPath.row]
        return cell
    }
}
