//
//  KeyboardViewController.swift
//  BaseApp
//
//  Created by Henderson on 2021/03/31.
//  Copyright © 2021 Henderson. All rights reserved.
//

import UIKit

class KeyboardViewController: BaseViewController, UITextFieldDelegate, UIScrollViewDelegate {
    
    //Constants for Keyboard
    let animationDuration = 0.25
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addListenerHideKeyboard(self.view)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /// UITextFieldDelegate
    var focusedTextField: UITextField? = nil
    func textFieldDidBeginEditing(_ textField: UITextField){
        focusedTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        UIView.animate(withDuration: animationDuration) { self.view.layer.frame.origin.y = 0 }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        
        guard let textField = focusedTextField else { return }
        let frame = textField.superview!.convert(textField.frame, to: nil)
        if (!aRect.contains(frame)){
            UIView.animate(withDuration: animationDuration) { self.view.layer.frame.origin.y = -keyboardSize!.height }
        }
    }
    
    /// body 터치 시에 키보드 닫히도록 이벤트 등록
    /// - Parameter view: 최상위 뷰
    func addListenerHideKeyboard(_ view : UIView){
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.clickedViewBody(sender:)))
        view.addGestureRecognizer(gesture)
        gesture.cancelsTouchesInView = false
    }
    
    /// body 터치 시에 키보드 닫히도록 이벤트 등록
    @objc func clickedViewBody(sender:UIGestureRecognizer) {
        self.view.endEditing(true)
    }
    

//    /// UIScrollViewDelegate
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView){
//        self.view.endEditing(true)
//    }
}
