//
//  BaseVC.swift
//  BaseApp
//
//  Created by Henderson on 2019/12/16.
//  Copyright © 2019 Henderson. All rights reserved.
//

import UIKit
import os
import RxSwift
import RxCocoa
import NVActivityIndicatorView

infix operator <-> : DefaultPrecedence

func <-> (property: ControlProperty<String?>, relay: BehaviorRelay<String>) -> Disposable {
    let bindToUIDisposable = relay.bind(to: property)
    let bindToRelay = property
        .subscribe(onNext: { n in
            relay.accept(n!)
        }, onCompleted:  {
            bindToUIDisposable.dispose()
        })
    
    return Disposables.create(bindToUIDisposable, bindToRelay)
}

class BaseViewController: UIViewController {
    let disposeBag = DisposeBag()
    
    //Constants for Bottom sheet
    let backgroundView = UIView()
    let bottomSheetDuration: TimeInterval = 0.25
    
    //About Indicator
    let indicatorWidth = 60.0
    let indicatorHeight = 60.0
    lazy var indicator: UIView = {
        let view = UIView(frame: self.view.frame)
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.alpha = 0.7
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        
        let frame = CGRect(x: self.view.frame.midX - indicatorWidth/2, y: self.view.frame.midY - indicatorHeight/2, width: indicatorWidth, height: indicatorHeight)
        let indicator = NVActivityIndicatorView(frame: frame, type: .circleStrokeSpin, color: dodgerBlue, padding: 0)
        indicator.startAnimating()
        
        view.addSubview(indicator)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BaseUtil.shared.printLog("viewDidLoad")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        BaseUtil.shared.printLog("viewWillAppear")
        hideNavigationBar(false)
    }
    
    /// 코딩으로 뷰를 만들어 넣을 땐 viewDidAppear에 구현해야함
    /// 이전 생애주기에서 .frame 을 활용해 뷰의 위치를 확인해본 결과 실제 화면과 차이가 발생
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        BaseUtil.shared.printLog("viewDidAppear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        BaseUtil.shared.printLog("viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        BaseUtil.shared.printLog("viewDidDisappear")
    }
    
    func showLoadingBar() {
        self.view.isUserInteractionEnabled = false
        //lazy이기 때문에 초기화 안된 상태에서 애니메이션 적용하면 indicator가 생기는 것도 애니메이션 적용되어, 1번은 초기화 시켜주기 위함
        let view = self.indicator
        UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.view.addSubview(view)
        }, completion: nil)
    }
    
    func hideLoadingBar() {
        UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.indicator.removeFromSuperview()
        }, completion: nil)
        self.view.isUserInteractionEnabled = true
    }
    
    /// For bottom sheet
    func addBackground() {
        setBackground()
        self.view.addSubview(backgroundView)
    }
    
    /// For bottom sheet
    func addBottomSheet(dialog: BaseBottomSheetDialog) {
        let height = view.frame.height
        let width  = view.frame.width
        dialog.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: width, height: height)
        self.addChild(dialog)
        dialog.didMove(toParent: self)
        self.view.addSubview(dialog.view)
        
        UIView.transition(with: self.view, duration: bottomSheetDuration, options: [.curveEaseIn], animations: {
            dialog.view.frame = CGRect(x: 0, y: 0, width: width, height: height)
        }, completion: nil)
    }
    
    func setBackground() {
        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        backgroundView.frame = self.view.frame
    }
    
    func hideNavigationBar(_ isHidden: Bool) {
        self.navigationController?.navigationBar.isHidden = isHidden
    }
    
    func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    func go(to: UIViewController) {
        self.navigationController?.pushViewController(to, animated: true)
    }
    
    func moveWithRemoving(to: UIViewController) {
        var viewArray = self.navigationController?.viewControllers
        viewArray!.removeLast()
        viewArray!.append(to)
        self.navigationController?.setViewControllers(viewArray!, animated: true)
    }
    
    func showNoPermissionAlert() {
        let alert = UIAlertController (title: "alert_setting_title".localized, message: "alert_setting_message".localized, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "alert_setting_ok".localized, style: .default) { (_) -> Void in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        }
        alert.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: "cancel".localized, style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        //해당 함수가 메인 스레드에서 동작하지 않을 수 있음(콜백으로 받으면)
        DispatchQueue.main.async { self.present(alert, animated: true, completion: nil) }
    }
}

extension UIViewController {
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 125, y: self.view.frame.size.height - 150, width: 250, height: 50))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = .systemFont(ofSize: 16.0)
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 1.0, delay: 2, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
