//
//  LoginVM.swift
//  BaseApp
//
//  Created by Henderson on 2021/04/6.
//  Copyright © 2020 Henderson. All rights reserved.
//

import Foundation
import RxCocoa
import LocalAuthentication


class LoginViewModel {
    var delegate: LoginDelegate? = nil
    
    var userId = BehaviorRelay<String>(value: "")
    var isChecked = BehaviorRelay<Bool>(value: false)
    var observedUserId = BehaviorRelay<String>(value: "")
    var userPassword = BehaviorRelay<String>(value: "")
    
    let authContext = LAContext()
    
    func clickLogin() {
        checkInputData()
    }
    
    func checkInputData() {
        if userId.value.isEmpty {
            delegate?.showDialog(message: "id_empty".localized)
        } else {
            checkAuth()
        }
    }
    
    /// 생체 인증을 지원하지 않으면 바로 callAPI() 호출, 지원하면 해당 인증 진행
    func checkAuth() {
        if authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            var message = ""
            switch authContext.biometryType {
            case .faceID: message = "Face ID로 인증해주세요."
            case .touchID : message = "Touch ID로 인증해주세요."
            case .none: message = "X"
            default: message = "X"
            }
            
            authContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: message) { (isSuccess, error) in
                if isSuccess {
                    DispatchQueue.main.async {
                        self.callAPI()
                    }
                } else {
                    if let error = error {
                        print("error : \(error)")
                    }
                }
            }
        } else {
            callAPI()
        }
    }
    
    func callAPI() {
        /* Example
         * let body = ["" : "",
         "" : ""]
         *  */
        let param =  [
            "osType" : "IOS",
            "packageVersion" : BaseUtil.shared.getVersionCode()
        ]
        
        delegate?.showLoadingBar()
        
        Http.shared.request(path: "apps/appMainMgt/webjson/appFailureResponse_IOS.webjson", method: .get, params: nil) { result, data in
            self.delegate?.hideLoadingBar()
            
            if !result {
                self.delegate?.showDialog(message: nil)
            } else {
                do {
                    let json = try JSONSerialization.data(withJSONObject: data)
                    let response = try JSONDecoder().decode(JsonResponse.self, from: json)
                    
                    if response.resultCode != 0 {
                        self.delegate?.showDialog(message: response.resultMsg)
                    } else {
                        self.delegate?.goToMain()
                    }
                } catch {
                    self.delegate?.goToMain()
//                    self.delegate?.showDialog(message: nil)
                }
            }
        }
    }
}

protocol LoginDelegate: BaseDelegate {
    func goToMain()
}
