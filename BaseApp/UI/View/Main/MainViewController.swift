//
//  MainViewController.swift
//  BaseApp
//
//  Created by Henderson on 2021/04/07.
//  Copyright © 2021 Henderson. All rights reserved.
//

import UIKit
import SwiftUI

class MainViewController: BaseViewController, BottomSheetDialogDelegate {
    
    let viewModel = MainViewModel()

    @IBOutlet weak var gpsButton: CustomButton!
    @IBOutlet weak var bleButton: CustomButton!
    @IBOutlet weak var webViewButton: CustomButton!
    @IBOutlet weak var appleLoginButton: CustomButton!
    @IBOutlet weak var alarmButton: CustomButton!
    @IBOutlet weak var swiftUIButton: CustomButton!
    @IBOutlet weak var bottomSheetButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var tabViewButton: UIButton!
    @IBOutlet weak var pageViewButton: UIButton!
    @IBOutlet weak var viewPager: ViewPager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initBinding()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setViewPager()
    }
    
    private func initBinding() {
        gpsButton.rx.tap.bind {
            self.go(to: GpsViewController())
        }.disposed(by: disposeBag)
        
        bleButton.rx.tap.bind {
            self.go(to: BleScanViewController())
        }.disposed(by: disposeBag)
        
        webViewButton.rx.tap.bind {
            self.go(to: WebViewController())
        }.disposed(by: disposeBag)
        
        swiftUIButton.rx.tap.bind {
            let swiftUIView = UIHostingController(rootView: ContentView())
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationItem.largeTitleDisplayMode = .always
            
            self.go(to: swiftUIView)
        }.disposed(by: disposeBag)
        
        appleLoginButton.rx.tap.bind {
            /// TODO: 애플 로그인 추구에 구현
        }.disposed(by: disposeBag)
        
        alarmButton.rx.tap.bind {
            BaseUtil.shared.playSound()
        }.disposed(by: disposeBag)
        
        bottomSheetButton.rx.tap.bind {
            self.showBottomSheet()
        }.disposed(by: disposeBag)
        
        nextButton.rx.tap.bind {
            self.go(to: ControlCircleViewController())
        }.disposed(by: disposeBag)
        
        tabViewButton.rx.tap.bind {
            self.go(to: TabViewController())
        }.disposed(by: disposeBag)
        
        pageViewButton.rx.tap.bind {
            self.go(to: PageViewController())
        }.disposed(by: disposeBag)
    }
    
    private func setViewPager() {
        let data = ["Hello", "World!"]
        self.viewPager.initBanner(data: data)
    }
    
    private func showBottomSheet() {
        addBackground()
        let dialog = BottomSheetDialog()
        dialog.delegate = self
        addBottomSheet(dialog: dialog)
    }
    
    /* InstallmentDialog Delegate */
    func dismissBottomSheet(dialog: UIViewController) {
        UIView.transition(with: self.view, duration: bottomSheetDuration, options: [.curveEaseIn], animations: {
            dialog.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: dialog.view.frame.width, height: dialog.view.frame.height)
        }, completion: {_ in
            self.backgroundView.removeFromSuperview()
            dialog.view.removeFromSuperview()
        })
    }
    
    func complete(data: Any?) {}
}
