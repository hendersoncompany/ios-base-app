//
//  BleControlViewController.swift
//  BaseApp
//
//  Created by Henderson on 2022/01/19.
//  Copyright © 2022 Henderson. All rights reserved.
//

import UIKit
import CoreBluetooth

class BleControlViewController: BaseViewController, BleDelegate {

    let viewModel = BleViewModel.shared
    
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var receivedData: UILabel!
    @IBOutlet weak var sendButton: RoundButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initBinding()
    }
    
    private func initView() {
        viewModel.delegate = self
        
        deviceName.text = viewModel.connectedGatt?.name
    }
    
    private func initBinding() {
        sendButton.rx.tap.bind {
            self.viewModel.writeData(command: .A)
        }.disposed(by: disposeBag)
    }
    
    func scannedDevice(device: CBPeripheral, rssi: NSNumber) { }
    
    func didConnect() { }
    
    func didDisconnect() {
        goBack()
    }
    
    func readData(command: CommandType, data: String) {
        receivedData.text = data
    }
}
