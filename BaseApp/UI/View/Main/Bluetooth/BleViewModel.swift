//
//  BleViewModel.swift
//  BaseApp
//
//  Created by Henderson on 2022/01/18.
//  Copyright © 2022 Henderson. All rights reserved.
//

import Foundation
import CoreBluetooth
import RxCocoa


enum CommandType {
    case A, B, C
}

protocol BleDelegate {
    func scannedDevice(device: CBPeripheral, rssi: NSNumber)
    func didConnect()
    func didDisconnect()
    func readData(command: CommandType, data: String)
}

/// BLE on/off에 대한 고찰
/// CBCentralManager()가 호출되면 centralManagerDidUpdateState가 콜백됨. 그리고 init된 후에 BLE on/off에 따라서도 centralManagerDidUpdateState가 콜백됨
/// init된 후에 on/off에 따라 해당 로직이 실행될 수 있으므로 centralManagerDidUpdateState안에 로직을 넣으면 안될 것으로 판단됨
class BleViewModel: NSObject {
    static let shared = BleViewModel()
    
    /// Feature for Ble
    var delegate: BleDelegate? = nil
    private var centralManager: CBCentralManager
    var isEnable = false
    
    /// Feature For Ble Scan
    private let scanPeriod: Double = 10
    var isScanning = BehaviorRelay<Bool>(value: false)

    /// Feature For Ble Connection, Read, Write Data
    var connectedGatt: CBPeripheral? = nil
    private var connectedChar: CBCharacteristic? = nil
    private let deviceGattUuid = CBUUID(string: "fec26ec4-6d71-4442-9f81-55bc21d658d0")
    private let deviceCharacteristicUuid = CBUUID(string: "fec26ec4-6d71-4442-9f81-55bc21d658d1")
    var isConnected = BehaviorRelay<Bool>(value: false)
    
    override init() {
        self.centralManager = CBCentralManager()
        super.init()
        
        self.centralManager.delegate = self
    }
}

extension BleViewModel: CBCentralManagerDelegate {
    
    func scanBle() {
        if isEnable {
            if isScanning.value {
                stopBleScan()
            } else {
                startBleScan()
            }
        } else {
            //BLE not enable
        }
    }
    
    private func startBleScan() {
        BaseUtil.shared.printLog("startBleScan")
        centralManager.scanForPeripherals(withServices: nil, options: nil)
        isScanning.accept(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + scanPeriod) { [self] in
            stopBleScan()
        }
    }
    
    private func stopBleScan() {
        BaseUtil.shared.printLog("stopBleScan")
        centralManager.stopScan()
        isScanning.accept(false)
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            isEnable = true
        } else {
            isEnable = false
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        BaseUtil.shared.printLog("didDiscover \(peripheral.name)")
        delegate?.scannedDevice(device: peripheral, rssi: RSSI)
    }
    
    func connectToDevice(device: CBPeripheral) {
        centralManager.connect(device, options: nil)
    }
    
    func disconnectDevice() {
        if let gatt = connectedGatt {
            centralManager.cancelPeripheralConnection(gatt)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        BaseUtil.shared.printLog("didConnect")
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        connectedGatt = peripheral
        isConnected.accept(true)
        delegate?.didConnect()
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        BaseUtil.shared.printLog("didDisconnectPeripheral")
        isConnected.accept(false)
        delegate?.didDisconnect()
    }
}

extension BleViewModel: CBPeripheralDelegate {
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        BaseUtil.shared.printLog("didDiscoverServices")
        if let services = peripheral.services {
            for service in services {
                if service.uuid == deviceGattUuid {
                    peripheral.discoverCharacteristics(nil, for: service)
                }
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        BaseUtil.shared.printLog("didDiscoverCharacteristicsFor")
        if let characteristic = service.characteristics {
            for char in characteristic {
                if char.uuid == deviceCharacteristicUuid {
                    connectedChar = char
                    connectedGatt?.readValue(for: connectedChar!)
                }
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        BaseUtil.shared.printLog("didUpdateValueFor")
        guard let data = characteristic.value else {
            return
        }
        let result = String(data: data, encoding: String.Encoding.utf8)
        readData(data: result!)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        /// 필요에 따라 setNotificationDescriptor의 콜백 설정
    }
    
    func writeData(command: CommandType) {
        BaseUtil.shared.printLog("writeData")
        
        //Temp Data
        let data = [0x02, 0x54, 0x00, 0x00, 0x00, 0x0D, 0x0A] as [UInt8]
        connectedGatt?.writeValue(Data(data), for: connectedChar!, type: .withoutResponse)
    }
    
    private func readData(data: String) {
        BaseUtil.shared.printLog("readData")
        delegate?.readData(command: CommandType.A, data: data)
    }
    
    /// 필요에 따라 true or false로 설정
    func setNotificationDescriptor() {
        connectedGatt?.setNotifyValue(false, for: connectedChar!)
    }
}
