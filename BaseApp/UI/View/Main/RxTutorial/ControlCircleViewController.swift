//
//  ControlCircleViewController.swift
//  BaseApp
//
//  Created by Henderson on 2021/06/18.
//  Copyright © 2021 Henderson. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa



/// cf: https://pilgwon.github.io/blog/2017/09/26/RxSwift-By-Examples-1-The-Basics.html
class ControlCircleViewController: BaseViewController {
    
    var circleView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initBinding()
    }
    
    private func initView() {
        circleView = UIView(frame: CGRect(origin: view.center, size: CGSize(width: 100, height: 100)))
        circleView.layer.cornerRadius = circleView.frame.width / 2
        circleView.center = view.center
        circleView.backgroundColor = .blue
        view.addSubview(circleView)
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(moveCircle))
        circleView.addGestureRecognizer(gesture)
    }
    
    private func initBinding() {
        circleView.rx.observe(CGPoint.self, "center")
            .asObservable()
            .subscribe(onNext: { location in
            self.view.backgroundColor = UIColor(red: Int(location!.x)%256, green: Int(location!.y)%256, blue: 255)
        })
        .disposed(by: disposeBag)
    }
    
    @objc private func moveCircle(_ recognizer: UIPanGestureRecognizer) {
        let location = recognizer.location(in: view)
        UIView.animate(withDuration: 0.1) {
            self.circleView.center = location
        }
    }
}
