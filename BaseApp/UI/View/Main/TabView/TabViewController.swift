//
//  TabViewController.swift
//  BaseApp
//
//  Created by Henderson on 2021/10/15.
//  Copyright © 2021 Henderson. All rights reserved.
//

import UIKit

class TabViewController: UITabBarController {
    
    var firstViewController : UIViewController!
    var secondViewController : UIViewController!
    var thirdViewController : UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTabBarSetting()
        setTabBarView()
        setTabBarIcon()
    }
    
    private func setTabBarSetting() {
        tabBar.backgroundColor = .white
        tabBar.tintColor = .black
    }
    
    private func setTabBarView() {
        firstViewController = FirstPageViewController()
        secondViewController = SecondPageViewController()
        thirdViewController = ThirdPageViewController()
            
        viewControllers = [
            firstViewController,
            secondViewController,
            thirdViewController
        ]
    }
    
    private func setTabBarIcon() {
        let item1 = UITabBarItem(title: "First", image: UIImage(named: "ic_checker"), tag: 0)
        let item2 = UITabBarItem(title: "Second", image:  UIImage(named: "ic_checker"), tag: 1)
        let item3 = UITabBarItem(title: "Third", image:  UIImage(named: "ic_checker"), tag: 2)
        
        firstViewController.tabBarItem = item1
        secondViewController.tabBarItem = item2
        thirdViewController.tabBarItem = item3
    }
}
