//
//  ScrollView.swift
//  BaseApp
//
//  Created by Henderson on 2021/04/22.
//  Copyright © 2021 Henderson. All rights reserved.
//

import Foundation
import UIKit

class ViewPager: UIScrollView {
    var timer: Timer? = nil
    var data: [String]? = nil
    
    var width: CGFloat { frame.size.width }
    var height: CGFloat { frame.size.height }
    var currentPage: Int { Int(round(self.contentOffset.x / self.frame.width)) }
    
    let bannerAnimationInterval: TimeInterval = 3
    
    deinit {
        timer?.invalidate()
    }
    
    func initBanner(data: [String]) {
        if timer == nil {
            self.data = data
            initView()
            timer = Timer.scheduledTimer(withTimeInterval: bannerAnimationInterval, repeats: true) { timer in
                self.goToNext()
            }
        }
    }
    
    func initView() {
        isScrollEnabled = false
        
        self.contentSize = CGSize(width: width * 5, height: height)

        for index in 0 ..< data!.count {
            let page = UILabel(frame: CGRect(x: CGFloat(index) * width, y: 0, width: width, height: height))
            page.backgroundColor = EAEAEA
            page.text = data![index]
            page.textAlignment = .center
            self.addSubview(page)
        }
    }
    
    func goToNext() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                if self.currentPage == self.data!.count-1 {
                    self.contentOffset.x = 0
                } else {
                    self.contentOffset.x += self.frame.size.width
                }
            }, completion: nil)
        }
    }
}
