//
//  String.swift
//  BaseApp
//
//  Created by Henderson on 2021/03/31.
//  Copyright © 2021 Henderson. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: "Localizable", value: self, comment: "")
    }
}
