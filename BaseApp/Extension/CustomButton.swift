//
//  Button.swift
//  BaseApp
//
//  Created by Henderson on 2021/03/31.
//  Copyright © 2021 Henderson. All rights reserved.
//

import Foundation
import UIKit


class RoundButton: CustomButton {
    private func setButton() {
        self.layer.cornerRadius = self.frame.height/2
    }
    
    override func awakeFromNib() {
        setButton()
    }
    
    override func prepareForInterfaceBuilder() {
        setButton()
    }
}

class CustomButton: UIButton {
    @IBInspectable
    var localisedKey: String? {
        didSet {
            guard let key = localisedKey else { return }
            UIView.performWithoutAnimation {
                setTitle(key.localized, for: .normal)
                layoutIfNeeded()
            }
        }
    }
    
    /// 텍스트 크기 통일하기 위함
    @IBInspectable
    var textFont: String {
        get {
            switch titleLabel?.font.pointSize {
            case 10:
                return "smallest"
            case 12:
                return "smaller"
            case 14:
                return "small"
            case 16:
                return "normal"
            case 18:
                return "big"
            case 20:
                return "bigger"
            case 22:
                return "biggest"
            default:
                return "biggest"
            }
        }
        set {
            switch newValue {
            case "smallest":
                titleLabel?.font = titleLabel?.font.withSize(FontSize.smallest.rawValue)
            case "smaller":
                titleLabel?.font = titleLabel?.font.withSize(FontSize.smaller.rawValue)
            case "small":
                titleLabel?.font = titleLabel?.font.withSize(FontSize.small.rawValue)
            case"normal":
                titleLabel?.font = titleLabel?.font.withSize(FontSize.normal.rawValue)
            case "big":
                titleLabel?.font = titleLabel?.font.withSize(FontSize.big.rawValue)
            case "bigger":
                titleLabel?.font = titleLabel?.font.withSize(FontSize.bigger.rawValue)
            case "biggest":
                titleLabel?.font = titleLabel?.font.withSize(FontSize.biggest.rawValue)
            default:
                titleLabel?.font = titleLabel?.font.withSize(FontSize.biggest.rawValue)
            }
        }
    }
    
    /// 테두리 색깔
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            layer.borderColor = newValue!.cgColor
        }
    }
    
    /// 테두리 두께
    @IBInspectable
    var borderWidth: CGFloat {
        get { return layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
}
