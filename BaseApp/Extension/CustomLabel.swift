//
//  Label.swift
//  BaseApp
//
//  Created by Henderson on 2021/04/01.
//  Copyright © 2021 Henderson. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
class CustomLabel: UILabel {
    /// 패딩
    var textInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }
    
    @IBInspectable
    var topTextInset: CGFloat {
        get { return textInsets.top }
        set { textInsets.top = newValue }
    }
    
    @IBInspectable
    var leftTextInset: CGFloat {
        get { return textInsets.left }
        set { textInsets.left = newValue }
    }
    
    @IBInspectable
    var rightTextInset: CGFloat {
        get { return textInsets.right }
        set { textInsets.right = newValue }
    }
    
    @IBInspectable
    var bottomTextInset: CGFloat {
        set { textInsets.bottom = newValue }
        get { return textInsets.bottom }
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = bounds.inset(by: textInsets)
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        let invertedInsets = UIEdgeInsets(top: -textInsets.top, left: -textInsets.left, bottom: -textInsets.bottom, right: -textInsets.right)
        return textRect.inset(by: invertedInsets)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: textInsets))
    }
    
    /// 테두리 색깔
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            layer.borderColor = newValue!.cgColor
        }
    }
    
    /// 테두리 두께
    @IBInspectable
    var borderWidth: CGFloat {
        get { return layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
    
    /// 텍스트 크기
    @IBInspectable
    var textFont: String {
        get {
            switch font.pointSize {
            case 10:
                return "smallest"
            case 12:
                return "smaller"
            case 14:
                return "small"
            case 16:
                return "normal"
            case 18:
                return "big"
            case 20:
                return "bigger"
            case 22:
                return "biggest"
            default:
                return "biggest"
            }
        }
        set {
            switch newValue {
            case "smallest":
                font = self.font.withSize(FontSize.smallest.rawValue)
            case "smaller":
                font = self.font.withSize(FontSize.smaller.rawValue)
            case "small":
                font = self.font.withSize(FontSize.small.rawValue)
            case"normal":
                font = self.font.withSize(FontSize.normal.rawValue)
            case "big":
                font = self.font.withSize(FontSize.big.rawValue)
            case "bigger":
                font = self.font.withSize(FontSize.bigger.rawValue)
            case "biggest":
                font = self.font.withSize(FontSize.biggest.rawValue)
            default:
                font = self.font.withSize(FontSize.biggest.rawValue)
            }
        }
    }
    
    /// 로컬 텍스트
    @IBInspectable
    var localizableText: String? {
        get { return text }
        set { text = NSLocalizedString(newValue!, comment: "") }
    }
}
