//
//  Dimen.swift
//  BaseApp
//
//  Created by Henderson on 2021/03/31.
//  Copyright © 2021 Henderson. All rights reserved.
//

import Foundation
import UIKit

enum FontSize: CGFloat {
    case smallest = 10
    case smaller = 12
    case small = 14
    case normal = 16
    case big = 18
    case bigger = 20
    case biggest = 22
}
