//
//  Http.swift
//  BaseApp
//
//  Created by Henderson on 2019/12/19.
//  Copyright © 2019 Henderson. All rights reserved.
//

import Foundation
import Alamofire

struct APIManager {
    static let manager: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 6
        configuration.timeoutIntervalForResource = 6
        return  Alamofire.Session(configuration: configuration)
    }()
}

final class Http {
    static let shared = Http()
    
    private let serverURL = "http://m.my.kt.com/"
  
    private init() {
    }
    
    func request(path: String, method: HTTPMethod, params: Parameters?, complete: @escaping(_ isResult: Bool, _ data: Any)-> Void) {
        let headers: HTTPHeaders = ["Content-Type" : "application/json;charset=UTF-8"]
        
        APIManager.manager.request(serverURL + path, method: method, parameters: params, encoding:JSONEncoding.default, headers : headers).responseJSON { response in
            
            switch response.result {
                case .success(let value):
                    complete(true, value)
                    
                case .failure(let error):
                    complete(false, error)
            }
        }
    }
}
