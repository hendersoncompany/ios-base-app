//
//  AppDelegate.swift
//  BaseApp
//
//  Created by Henderson on 2019/11/29.
//  Copyright © 2019 Henderson. All rights reserved.
//

import UIKit
import FirebaseCore
import UserNotifications
import AlamofireNetworkActivityLogger
import CoreData
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setFcm(application)
        setHttpLogger()
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let next = LoginViewController()
        let navigationController = UINavigationController(rootViewController: next)
        window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
        
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
        UserDefaults.shared.set(0, forKey: "notificationBadge")
    }
    
    /// Delegate for Deep Link
    /// Targets -> Info -> URL Types -> URL Scheme의 값으로 웹에서 호출하면 아래의 함수가 호출됨(ex: baselink://test.com?id=1
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return true
    }
    
    /// Set TitleBar for common using
    private func setTitleBar(){
        //        UINavigationBar.appearance().barTintColor = UIColor(red: 30/255.0, green: 38/255.0, blue: 42/255.0, alpha: 1.0)
        //        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().backgroundColor = .clear
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "DB")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    /// import FirebaseCore
    private func setFcm(_ application: UIApplication) {
        FirebaseApp.configure()
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { _, _ in })
     
        //request Notification Permission
        application.registerForRemoteNotifications()
        
        // If you want to get the fcm token in VC, use below code
        // import FirebaseMessaging
         Messaging.messaging().fcmToken
    }
    
    private func setHttpLogger() {
        #if DEBUG
            NetworkActivityLogger.shared.level = .debug
            NetworkActivityLogger.shared.startLogging()
        #endif
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        BaseUtil.shared.printLog("data: \(userInfo)")
        completionHandler([[.alert, .sound]])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        //data payload
        let userInfo = response.notification.request.content.userInfo
        BaseUtil.shared.printLog("data: \(userInfo)")
        completionHandler()
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        BaseUtil.shared.printLog("FCM token: \(fcmToken)")
        /*
         토큰을 얻고 싶을 때 아래의 함수를 이용해서 토큰을 얻을 수 있음
         Messaging.messaging().token { token, error in
         if let error = error {
         BaseUtil.shared.printLog("error: \(error)")
         } else if let token = token {
         BaseUtil.shared.printLog("token: \(token)")
         }
         }
         */
    }
}
