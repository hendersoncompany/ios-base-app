//
//  ResponseResult.swift
//  BaseApp
//
//  Created by Henderson on 2019/12/20.
//  Copyright © 2019 Henderson. All rights reserved.
//

import Foundation

class BaseResponse: Codable {
    /// Common
    var resultCode: Int
    var resultMsg: String
}
