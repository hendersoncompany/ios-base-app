//
//  NotificationService.swift
//  BaseServiceExtension
//
//  Created by henderson on 2022/06/08.
//  Copyright © 2022 Henderson. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var notificationContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        notificationContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let content = notificationContent {
            let badgeCount = UserDefaults(suiteName: "group.com.ant.test")!.integer(forKey: "notificationBadge") + 1
            UserDefaults(suiteName: "group.com.ant.test")!.set(badgeCount, forKey: "notificationBadge")
            
            content.badge = badgeCount as NSNumber
            content.title = "\(content.title) [Modified]"
            
            contentHandler(content)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  notificationContent {
            contentHandler(bestAttemptContent)
        }
    }
}
